package com.automationanywhere.botcommand.demo;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Double;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class RemoveColorRangeCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(RemoveColorRangeCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    RemoveColorRange command = new RemoveColorRange();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("InputFilePath") && parameters.get("InputFilePath") != null && parameters.get("InputFilePath").get() != null) {
      convertedParameters.put("InputFilePath", parameters.get("InputFilePath").get());
      if(!(convertedParameters.get("InputFilePath") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","InputFilePath", "String", parameters.get("InputFilePath").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("InputFilePath") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","InputFilePath"));
    }

    if(parameters.containsKey("OutputFilePath") && parameters.get("OutputFilePath") != null && parameters.get("OutputFilePath").get() != null) {
      convertedParameters.put("OutputFilePath", parameters.get("OutputFilePath").get());
      if(!(convertedParameters.get("OutputFilePath") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","OutputFilePath", "String", parameters.get("OutputFilePath").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("OutputFilePath") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","OutputFilePath"));
    }

    if(parameters.containsKey("LowerRed") && parameters.get("LowerRed") != null && parameters.get("LowerRed").get() != null) {
      convertedParameters.put("LowerRed", parameters.get("LowerRed").get());
      if(!(convertedParameters.get("LowerRed") instanceof Double)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","LowerRed", "Double", parameters.get("LowerRed").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("LowerRed") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","LowerRed"));
    }

    if(parameters.containsKey("HigherRed") && parameters.get("HigherRed") != null && parameters.get("HigherRed").get() != null) {
      convertedParameters.put("HigherRed", parameters.get("HigherRed").get());
      if(!(convertedParameters.get("HigherRed") instanceof Double)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","HigherRed", "Double", parameters.get("HigherRed").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("HigherRed") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","HigherRed"));
    }

    if(parameters.containsKey("LowerBlue") && parameters.get("LowerBlue") != null && parameters.get("LowerBlue").get() != null) {
      convertedParameters.put("LowerBlue", parameters.get("LowerBlue").get());
      if(!(convertedParameters.get("LowerBlue") instanceof Double)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","LowerBlue", "Double", parameters.get("LowerBlue").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("LowerBlue") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","LowerBlue"));
    }

    if(parameters.containsKey("HigherBlue") && parameters.get("HigherBlue") != null && parameters.get("HigherBlue").get() != null) {
      convertedParameters.put("HigherBlue", parameters.get("HigherBlue").get());
      if(!(convertedParameters.get("HigherBlue") instanceof Double)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","HigherBlue", "Double", parameters.get("HigherBlue").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("HigherBlue") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","HigherBlue"));
    }

    if(parameters.containsKey("LowerGreen") && parameters.get("LowerGreen") != null && parameters.get("LowerGreen").get() != null) {
      convertedParameters.put("LowerGreen", parameters.get("LowerGreen").get());
      if(!(convertedParameters.get("LowerGreen") instanceof Double)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","LowerGreen", "Double", parameters.get("LowerGreen").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("LowerGreen") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","LowerGreen"));
    }

    if(parameters.containsKey("HigherGreen") && parameters.get("HigherGreen") != null && parameters.get("HigherGreen").get() != null) {
      convertedParameters.put("HigherGreen", parameters.get("HigherGreen").get());
      if(!(convertedParameters.get("HigherGreen") instanceof Double)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","HigherGreen", "Double", parameters.get("HigherGreen").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("HigherGreen") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","HigherGreen"));
    }

    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("InputFilePath"),(String)convertedParameters.get("OutputFilePath"),(Double)convertedParameters.get("LowerRed"),(Double)convertedParameters.get("HigherRed"),(Double)convertedParameters.get("LowerBlue"),(Double)convertedParameters.get("HigherBlue"),(Double)convertedParameters.get("LowerGreen"),(Double)convertedParameters.get("HigherGreen")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
