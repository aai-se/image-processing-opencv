package com.automationanywhere.botcommand.demo;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Number;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class AddLabelToDocumentCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(AddLabelToDocumentCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    AddLabelToDocument command = new AddLabelToDocument();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("InputFilePath") && parameters.get("InputFilePath") != null && parameters.get("InputFilePath").get() != null) {
      convertedParameters.put("InputFilePath", parameters.get("InputFilePath").get());
      if(!(convertedParameters.get("InputFilePath") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","InputFilePath", "String", parameters.get("InputFilePath").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("InputFilePath") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","InputFilePath"));
    }

    if(parameters.containsKey("OutputFilePath") && parameters.get("OutputFilePath") != null && parameters.get("OutputFilePath").get() != null) {
      convertedParameters.put("OutputFilePath", parameters.get("OutputFilePath").get());
      if(!(convertedParameters.get("OutputFilePath") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","OutputFilePath", "String", parameters.get("OutputFilePath").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("OutputFilePath") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","OutputFilePath"));
    }

    if(parameters.containsKey("x") && parameters.get("x") != null && parameters.get("x").get() != null) {
      convertedParameters.put("x", parameters.get("x").get());
      if(!(convertedParameters.get("x") instanceof Number)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","x", "Number", parameters.get("x").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("x") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","x"));
    }

    if(parameters.containsKey("y") && parameters.get("y") != null && parameters.get("y").get() != null) {
      convertedParameters.put("y", parameters.get("y").get());
      if(!(convertedParameters.get("y") instanceof Number)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","y", "Number", parameters.get("y").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("y") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","y"));
    }

    if(parameters.containsKey("PageNum") && parameters.get("PageNum") != null && parameters.get("PageNum").get() != null) {
      convertedParameters.put("PageNum", parameters.get("PageNum").get());
      if(!(convertedParameters.get("PageNum") instanceof Number)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","PageNum", "Number", parameters.get("PageNum").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("PageNum") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","PageNum"));
    }

    if(parameters.containsKey("LabelName") && parameters.get("LabelName") != null && parameters.get("LabelName").get() != null) {
      convertedParameters.put("LabelName", parameters.get("LabelName").get());
      if(!(convertedParameters.get("LabelName") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","LabelName", "String", parameters.get("LabelName").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("LabelName") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","LabelName"));
    }

    if(parameters.containsKey("LabelValue") && parameters.get("LabelValue") != null && parameters.get("LabelValue").get() != null) {
      convertedParameters.put("LabelValue", parameters.get("LabelValue").get());
      if(!(convertedParameters.get("LabelValue") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","LabelValue", "String", parameters.get("LabelValue").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("LabelValue") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","LabelValue"));
    }

    if(parameters.containsKey("TextHexColor") && parameters.get("TextHexColor") != null && parameters.get("TextHexColor").get() != null) {
      convertedParameters.put("TextHexColor", parameters.get("TextHexColor").get());
      if(!(convertedParameters.get("TextHexColor") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","TextHexColor", "String", parameters.get("TextHexColor").get().getClass().getSimpleName()));
      }
    }

    if(parameters.containsKey("FontSize") && parameters.get("FontSize") != null && parameters.get("FontSize").get() != null) {
      convertedParameters.put("FontSize", parameters.get("FontSize").get());
      if(!(convertedParameters.get("FontSize") instanceof Number)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","FontSize", "Number", parameters.get("FontSize").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("FontSize") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","FontSize"));
    }

    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("InputFilePath"),(String)convertedParameters.get("OutputFilePath"),(Number)convertedParameters.get("x"),(Number)convertedParameters.get("y"),(Number)convertedParameters.get("PageNum"),(String)convertedParameters.get("LabelName"),(String)convertedParameters.get("LabelValue"),(String)convertedParameters.get("TextHexColor"),(Number)convertedParameters.get("FontSize")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
