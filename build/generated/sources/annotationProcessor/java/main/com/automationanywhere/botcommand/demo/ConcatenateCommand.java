package com.automationanywhere.botcommand.demo;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class ConcatenateCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(ConcatenateCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    Concatenate command = new Concatenate();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("InputFilePath1") && parameters.get("InputFilePath1") != null && parameters.get("InputFilePath1").get() != null) {
      convertedParameters.put("InputFilePath1", parameters.get("InputFilePath1").get());
      if(!(convertedParameters.get("InputFilePath1") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","InputFilePath1", "String", parameters.get("InputFilePath1").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("InputFilePath1") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","InputFilePath1"));
    }

    if(parameters.containsKey("InputFilePath2") && parameters.get("InputFilePath2") != null && parameters.get("InputFilePath2").get() != null) {
      convertedParameters.put("InputFilePath2", parameters.get("InputFilePath2").get());
      if(!(convertedParameters.get("InputFilePath2") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","InputFilePath2", "String", parameters.get("InputFilePath2").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("InputFilePath2") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","InputFilePath2"));
    }

    if(parameters.containsKey("OutputFilePath") && parameters.get("OutputFilePath") != null && parameters.get("OutputFilePath").get() != null) {
      convertedParameters.put("OutputFilePath", parameters.get("OutputFilePath").get());
      if(!(convertedParameters.get("OutputFilePath") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","OutputFilePath", "String", parameters.get("OutputFilePath").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("OutputFilePath") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","OutputFilePath"));
    }

    if(parameters.containsKey("ConcatenateType") && parameters.get("ConcatenateType") != null && parameters.get("ConcatenateType").get() != null) {
      convertedParameters.put("ConcatenateType", parameters.get("ConcatenateType").get());
      if(!(convertedParameters.get("ConcatenateType") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","ConcatenateType", "String", parameters.get("ConcatenateType").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("ConcatenateType") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","ConcatenateType"));
    }
    if(convertedParameters.get("ConcatenateType") != null) {
      switch((String)convertedParameters.get("ConcatenateType")) {
        case "VERTICAL" : {

        } break;
        case "HORIZONTAL" : {

        } break;
        default : throw new BotCommandException(MESSAGES_GENERIC.getString("generic.InvalidOption","ConcatenateType"));
      }
    }

    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("InputFilePath1"),(String)convertedParameters.get("InputFilePath2"),(String)convertedParameters.get("OutputFilePath"),(String)convertedParameters.get("ConcatenateType")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
