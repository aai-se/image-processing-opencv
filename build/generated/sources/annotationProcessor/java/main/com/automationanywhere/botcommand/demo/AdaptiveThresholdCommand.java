package com.automationanywhere.botcommand.demo;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.Boolean;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class AdaptiveThresholdCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(AdaptiveThresholdCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    AdaptiveThreshold command = new AdaptiveThreshold();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("InputFilePath") && parameters.get("InputFilePath") != null && parameters.get("InputFilePath").get() != null) {
      convertedParameters.put("InputFilePath", parameters.get("InputFilePath").get());
      if(!(convertedParameters.get("InputFilePath") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","InputFilePath", "String", parameters.get("InputFilePath").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("InputFilePath") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","InputFilePath"));
    }

    if(parameters.containsKey("OutputFilePath") && parameters.get("OutputFilePath") != null && parameters.get("OutputFilePath").get() != null) {
      convertedParameters.put("OutputFilePath", parameters.get("OutputFilePath").get());
      if(!(convertedParameters.get("OutputFilePath") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","OutputFilePath", "String", parameters.get("OutputFilePath").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("OutputFilePath") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","OutputFilePath"));
    }

    if(parameters.containsKey("ApplyBlur") && parameters.get("ApplyBlur") != null && parameters.get("ApplyBlur").get() != null) {
      convertedParameters.put("ApplyBlur", parameters.get("ApplyBlur").get());
      if(!(convertedParameters.get("ApplyBlur") instanceof Boolean)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","ApplyBlur", "Boolean", parameters.get("ApplyBlur").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("ApplyBlur") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","ApplyBlur"));
    }

    if(parameters.containsKey("InvertImage") && parameters.get("InvertImage") != null && parameters.get("InvertImage").get() != null) {
      convertedParameters.put("InvertImage", parameters.get("InvertImage").get());
      if(!(convertedParameters.get("InvertImage") instanceof Boolean)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","InvertImage", "Boolean", parameters.get("InvertImage").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("InvertImage") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","InvertImage"));
    }

    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("InputFilePath"),(String)convertedParameters.get("OutputFilePath"),(Boolean)convertedParameters.get("ApplyBlur"),(Boolean)convertedParameters.get("InvertImage")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
