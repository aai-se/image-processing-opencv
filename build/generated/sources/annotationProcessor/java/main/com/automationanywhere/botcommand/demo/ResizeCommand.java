package com.automationanywhere.botcommand.demo;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Double;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class ResizeCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(ResizeCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    Resize command = new Resize();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("InputFilePath") && parameters.get("InputFilePath") != null && parameters.get("InputFilePath").get() != null) {
      convertedParameters.put("InputFilePath", parameters.get("InputFilePath").get());
      if(!(convertedParameters.get("InputFilePath") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","InputFilePath", "String", parameters.get("InputFilePath").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("InputFilePath") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","InputFilePath"));
    }

    if(parameters.containsKey("OutputFilePath") && parameters.get("OutputFilePath") != null && parameters.get("OutputFilePath").get() != null) {
      convertedParameters.put("OutputFilePath", parameters.get("OutputFilePath").get());
      if(!(convertedParameters.get("OutputFilePath") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","OutputFilePath", "String", parameters.get("OutputFilePath").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("OutputFilePath") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","OutputFilePath"));
    }

    if(parameters.containsKey("ResizeMode") && parameters.get("ResizeMode") != null && parameters.get("ResizeMode").get() != null) {
      convertedParameters.put("ResizeMode", parameters.get("ResizeMode").get());
      if(!(convertedParameters.get("ResizeMode") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","ResizeMode", "String", parameters.get("ResizeMode").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("ResizeMode") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","ResizeMode"));
    }
    if(convertedParameters.get("ResizeMode") != null) {
      switch((String)convertedParameters.get("ResizeMode")) {
        case "ABSOLUTE" : {
          if(parameters.containsKey("newWidth") && parameters.get("newWidth") != null && parameters.get("newWidth").get() != null) {
            convertedParameters.put("newWidth", parameters.get("newWidth").get());
            if(!(convertedParameters.get("newWidth") instanceof Double)) {
              throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","newWidth", "Double", parameters.get("newWidth").get().getClass().getSimpleName()));
            }
          }

          if(parameters.containsKey("newHeight") && parameters.get("newHeight") != null && parameters.get("newHeight").get() != null) {
            convertedParameters.put("newHeight", parameters.get("newHeight").get());
            if(!(convertedParameters.get("newHeight") instanceof Double)) {
              throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","newHeight", "Double", parameters.get("newHeight").get().getClass().getSimpleName()));
            }
          }


        } break;
        case "RELATIVE" : {
          if(parameters.containsKey("newWidthRel") && parameters.get("newWidthRel") != null && parameters.get("newWidthRel").get() != null) {
            convertedParameters.put("newWidthRel", parameters.get("newWidthRel").get());
            if(!(convertedParameters.get("newWidthRel") instanceof Double)) {
              throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","newWidthRel", "Double", parameters.get("newWidthRel").get().getClass().getSimpleName()));
            }
          }

          if(parameters.containsKey("newHeightRel") && parameters.get("newHeightRel") != null && parameters.get("newHeightRel").get() != null) {
            convertedParameters.put("newHeightRel", parameters.get("newHeightRel").get());
            if(!(convertedParameters.get("newHeightRel") instanceof Double)) {
              throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","newHeightRel", "Double", parameters.get("newHeightRel").get().getClass().getSimpleName()));
            }
          }


        } break;
        default : throw new BotCommandException(MESSAGES_GENERIC.getString("generic.InvalidOption","ResizeMode"));
      }
    }

    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("InputFilePath"),(String)convertedParameters.get("OutputFilePath"),(String)convertedParameters.get("ResizeMode"),(Double)convertedParameters.get("newWidth"),(Double)convertedParameters.get("newHeight"),(Double)convertedParameters.get("newWidthRel"),(Double)convertedParameters.get("newHeightRel")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
