package com.automationanywhere.botcommand.demo;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class ImagesToPdfCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(ImagesToPdfCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    ImagesToPdf command = new ImagesToPdf();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("InputFilePaths") && parameters.get("InputFilePaths") != null && parameters.get("InputFilePaths").get() != null) {
      convertedParameters.put("InputFilePaths", parameters.get("InputFilePaths").get());
      if(!(convertedParameters.get("InputFilePaths") instanceof ArrayList)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","InputFilePaths", "ArrayList", parameters.get("InputFilePaths").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("InputFilePaths") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","InputFilePaths"));
    }

    if(parameters.containsKey("OutputFilePath") && parameters.get("OutputFilePath") != null && parameters.get("OutputFilePath").get() != null) {
      convertedParameters.put("OutputFilePath", parameters.get("OutputFilePath").get());
      if(!(convertedParameters.get("OutputFilePath") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","OutputFilePath", "String", parameters.get("OutputFilePath").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("OutputFilePath") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","OutputFilePath"));
    }

    try {
      Optional<Value> result =  Optional.ofNullable(command.action((ArrayList<Value>)convertedParameters.get("InputFilePaths"),(String)convertedParameters.get("OutputFilePath")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
