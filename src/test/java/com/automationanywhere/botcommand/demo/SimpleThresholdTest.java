package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class SimpleThresholdTest {

    SimpleThreshold command = new SimpleThreshold();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{
                {"C:\\OpenCV_AA\\input_images\\1.tiff", "C:\\OpenCV_AA\\output_images\\1_OUT_THRES.tiff",153.0,true,false, "true"},
                {"C:\\OpenCV_AA\\input_images\\a1.pdf", "C:\\OpenCV_AA\\output_images\\a1_OUT_THRES.pdf",153.0,true,false, "true"}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String InputFilePath, String OutputFilePath, Double ThresholdVal,Boolean Blur, Boolean INV, String Results){

       // BooleanValue d = command.action(InputFilePath, OutputFilePath,ThresholdVal,Blur, INV);
        //System.out.println("DEBUG:"+d.toString());
        //assertEquals(d.toString(),Results);
        assertEquals(true,true);
    }
}
