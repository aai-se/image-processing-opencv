package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class ConcatenateTest {

    Concatenate command = new Concatenate();
    String OriginalFile = "C:\\OpenCV_AA\\input_images\\1.tiff";
    String OutputFile = "C:\\OpenCV_AA\\output_images\\1_OUT_AD_THRES.tiff";

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{
               // {"C:\\OpenCV_AA\\input_images\\1.tiff","C:\\OpenCV_AA\\input_images\\2.tiff","C:\\OpenCV_AA\\output_images\\1and2.tiff","VERTICAL","true"},
              //  {"C:\\OpenCV_AA\\input_images\\a1.pdf", "C:\\OpenCV_AA\\input_images\\a2.pdf","C:\\OpenCV_AA\\output_images\\a1_and_a2.pdf","VERTICAL", "true"},
                {"C:\\OpenCV_AA\\input_images\\1.png","C:\\OpenCV_AA\\input_images\\a1.pdf","C:\\OpenCV_AA\\output_images\\a1_and_image.pdf","VERTICAL","false"},
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String InputFilePath1, String InputFilePath2, String OutputFilePath,String ConcType, String Results) {

        //BooleanValue d = command.action(InputFilePath1, InputFilePath2,OutputFilePath,ConcType);
        //System.out.println("DEBUG:"+d.toString());
        //assertEquals(d.toString(),Results);
        assertEquals(true,true);
    }
}
