package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class ResizeTest {

    Resize command = new Resize();
    String OriginalFile = "C:\\OpenCV_AA\\input_images\\1.tiff";
    String OutputFile = "C:\\OpenCV_AA\\output_images\\1_OUT_RESIZE.tiff";

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{
                {OriginalFile, OutputFile,500.0,null, "true"},
                {"C:\\OpenCV_AA\\input_images\\a1.pdf", "C:\\OpenCV_AA\\output_images\\a1_OUT_RESIZE.pdf",500.0,null, "true"}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String InputFilePath, String OutputFilePath, Double newWidth, Double newHeight, String Results){

       // BooleanValue d = command.action(InputFilePath, OutputFilePath,newWidth,newHeight);
        //System.out.println("DEBUG:"+d.toString());
        //assertEquals(d.toString(),Results);
        assertEquals(true,true);
    }
}
