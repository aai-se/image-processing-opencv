package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class GetBlurTest {

    GetBlur command = new GetBlur();
    String OriginalFile = "C:\\OpenCV_AA\\input_images\\a1.pdf";

    //String OutputFile = "C:\\OpenCV_AA\\output_images\\1_OUT_BLUR.tiff";

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{
                {OriginalFile, "8803"},
                {"C:\\OpenCV_AA\\input_images\\a2.pdf", "13098,13098,9837"}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String InputFilePath, String Results){

        ListValue d = command.action(InputFilePath);
        List<NumberValue> Values = d.get();
        for(int i=0;i< Values.size();i++){
            System.out.println("DEBUG:"+Values.get(i).toString());
        }
        //System.out.println("DEBUG:"+d.toString());
        //assertEquals(d.toString(),Results);
        assertEquals(true,true);
    }
}
