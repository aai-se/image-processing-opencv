package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class DenoiseTest {

    Denoise command = new Denoise();
    String OriginalFile = "C:\\OpenCV_AA\\input_images\\1.tiff";
    String OutputFile = "C:\\OpenCV_AA\\output_images\\1_OUT_DENOISE.tiff";

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{
                {OriginalFile, OutputFile,3.0, "true"},
                {"C:\\OpenCV_AA\\input_images\\a1.pdf", "C:\\OpenCV_AA\\output_images\\a1_OUT_DENOISE.pdf",3.0, "true"}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String InputFilePath, String OutputFilePath, Double Level, String Results){

        //BooleanValue d = command.action(InputFilePath, OutputFilePath,Level);
        //System.out.println("DEBUG:"+d.toString());
        //assertEquals(d.toString(),Results);
        assertEquals(true,true);
    }
}
