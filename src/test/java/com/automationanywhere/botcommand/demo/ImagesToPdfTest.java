package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class ImagesToPdfTest {

    ImagesToPdf command = new ImagesToPdf();
    List<StringValue> ImgList0 = new ArrayList<StringValue>();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){
        ImgList0.add(new StringValue("C:\\iqbot\\combined_images\\1.png"));
        ImgList0.add(new StringValue("C:\\iqbot\\combined_images\\2.png"));

        String OutputPDF = "C:\\iqbot\\combined_images\\Combined_doc.pdf";
        return new Object[][]{
                {ImgList0,OutputPDF}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(ArrayList<Value> InputFilePaths, String OutputFilePath){

        BooleanValue d = command.action(InputFilePaths, OutputFilePath);
        System.out.println("DEBUG:"+OutputFilePath);
        //assertEquals(d.toString(),Results);
        assertEquals(true,true);
    }
}
