package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class AddLabelToDocumentTest {

    AddLabelToDocument command = new AddLabelToDocument();
    String OriginalFile = "C:\\OpenCV_AA\\input_images\\a2.pdf";
    String OutputFile = "C:\\OpenCV_AA\\output_images\\a2_OUT.pdf";

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{
                {OriginalFile, OutputFile,50,50,3,"filename:",OriginalFile,"#0f0e0a",50}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String InputFilePath, String OutputFilePath, Number X, Number Y, Number PageNum, String LabelName, String LabelValue, String HexColor, Number FontSize){

        BooleanValue d = command.action(InputFilePath, OutputFilePath,X,Y,PageNum,LabelName, LabelValue, HexColor,FontSize);
        //System.out.println("DEBUG:"+d.toString());
        //assertEquals(d.toString(),Results);
        assertEquals(true,true);
    }
}
