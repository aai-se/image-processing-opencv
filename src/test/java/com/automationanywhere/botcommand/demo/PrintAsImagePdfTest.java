package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class PrintAsImagePdfTest {

    PrintAsImagePdf command = new PrintAsImagePdf();
    String OriginalFile = "C:\\OpenCV_AA\\input_images\\1.tiff";
    String OutputFile = "C:\\OpenCV_AA\\output_images\\1_OUT_BLUR.tiff";

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{
                //{OriginalFile, OutputFile, "true"},
                {"C:\\OpenCV_AA\\input_images\\a2.pdf", "C:\\OpenCV_AA\\output_images\\a2_AS_IMG.pdf", "true"}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String InputFilePath, String OutputFilePath, String Results){

       // BooleanValue d = command.action(InputFilePath, OutputFilePath);
        //System.out.println("DEBUG:"+d.toString());
        //assertEquals(d.toString(),Results);
        assertEquals(true,true);
    }
}
