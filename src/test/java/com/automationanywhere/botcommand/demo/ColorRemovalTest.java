package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class ColorRemovalTest {

    RemoveColorRange command = new RemoveColorRange();
    String OriginalFile = "C:\\iqbot\\color_images\\test3.png";
    String OutputFile = "C:\\iqbot\\color_images\\test3_OUT.png";

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{
                {OriginalFile, OutputFile,170.0,255.0,20.0,255.0,20.0,255.0}

        };
    }

    @Test(dataProvider = "data")
    public void aTests(String InputFilePath, String OutputFilePath, Double LowerRed, Double HigherRed, Double LowerBlue, Double HigherBlue, Double LowerGreen, Double HigherGreen){

        BooleanValue d = command.action(InputFilePath, OutputFilePath,LowerRed, HigherRed,LowerBlue, HigherBlue,LowerGreen, HigherGreen);
        //System.out.println("DEBUG:"+d.toString());
        //assertEquals(d.toString(),Results);
        assertEquals(true,true);
    }
}
