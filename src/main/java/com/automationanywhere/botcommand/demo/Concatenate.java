/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.cvlogics.CommonCVOperations;
import com.automationanywhere.utils.DllLoader;
import com.automationanywhere.utils.FileProcessor;

import java.util.ArrayList;

/**
 * @author Bren Sapience
 *
 */
@BotCommand
@CommandPkg(label="Concatenate", name="Concatenate", description="Concatenate Images or PDFs", icon="pkg.svg",
		node_label="Concatenate",
		return_type= DataType.BOOLEAN, return_label="Assign the output to variable", return_required=true)

public class Concatenate {

	private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

	@Execute
	public BooleanValue action (@Idx(index = "1", type = AttributeType.FILE)  @Pkg(label = "Input File Path 1" , default_value_type =  DataType.FILE) @NotEmpty String InputFilePath1,
						@Idx(index = "2", type = AttributeType.FILE)  @Pkg(label = "Input File Path 2" , default_value_type =  DataType.FILE) @NotEmpty String InputFilePath2,
						@Idx(index = "3", type = AttributeType.FILE)  @Pkg(label = "Output File Path" , default_value_type =  DataType.FILE) @NotEmpty String OutputFilePath,
						@Idx(index = "4", type = AttributeType.SELECT, options = {
								@Idx.Option(index = "4.1", pkg = @Pkg(label = "Vertical", value = "VERTICAL")),
								@Idx.Option(index = "4.2", pkg = @Pkg(label = "Horizontal", value = "HORIZONTAL")),
						}) @Pkg(label = "Concatenate Type", default_value = "VERTICAL", default_value_type = DataType.STRING) @NotEmpty String ConcatenateType)
	{


		DllLoader l = new DllLoader();//Load OpenCV DLL

		FileProcessor fp1 = new FileProcessor(InputFilePath1);
		ArrayList<String> AllImages1 = fp1.GetFileAsImages();

		FileProcessor fp2 = new FileProcessor(InputFilePath2);
		ArrayList<String> AllImages2 = fp2.GetFileAsImages();


		if(fp1.isFileAPdf() && fp2.isFileAPdf()){
			// actual OpenCV Logic
			ArrayList<String> AllImages = new ArrayList<String>();
			AllImages.addAll(AllImages1);
			AllImages.addAll(AllImages2);

			boolean res = fp1.CombineImagesToPdf(AllImages,OutputFilePath);
			fp1.Cleanup();
			fp2.Cleanup();
			return new BooleanValue(Boolean.toString(res));

		}else if (!fp1.isFileAPdf() && !fp2.isFileAPdf()){ // Concatenate 2 images

			boolean res = CommonCVOperations.ConcatenateImages(ConcatenateType,InputFilePath1,InputFilePath2,OutputFilePath);
			fp1.Cleanup();
			fp2.Cleanup();
			return new BooleanValue(Boolean.toString(res));

		}else{ // Concatenate an image + PDF or PDF + Image = PDF
			// actual OpenCV Logic
			ArrayList<String> AllImages = new ArrayList<String>();
			AllImages.addAll(AllImages1);
			AllImages.addAll(AllImages2);
			//System.out.println("DEBUG: Size: "+AllImages.size());
			boolean res = fp1.CombineImagesToPdf(AllImages,OutputFilePath);
			System.out.println("DEBUG Res:"+res);
			fp1.Cleanup();
			fp2.Cleanup();

			return new BooleanValue(Boolean.toString(res));
		}
	}

}
