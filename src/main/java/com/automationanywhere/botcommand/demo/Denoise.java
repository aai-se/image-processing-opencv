/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.cvlogics.CommonCVOperations;
import com.automationanywhere.utils.DllLoader;
import com.automationanywhere.utils.FileProcessor;

import java.util.ArrayList;

/**
 * @author Bren Sapience
 *
 */
@BotCommand
@CommandPkg(label="Denoise", name="Denoise", description="Denoise Image", icon="pkg.svg",
		node_label="Denoise",
		return_type= DataType.BOOLEAN, return_label="Assign the output to variable", return_required=true)

public class Denoise {

	private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

	@Execute
	public BooleanValue action(
			@Idx(index = "1", type = AttributeType.FILE) @Pkg(label = "Input File Path", default_value_type = DataType.FILE) @NotEmpty String InputFilePath,
			@Idx(index = "2", type = AttributeType.FILE) @Pkg(label = "Output File Path", default_value_type = DataType.FILE) @NotEmpty String OutputFilePath,
			@Idx(index = "3", type = AttributeType.NUMBER) @Pkg(label = " Filter Strength"  , default_value_type = DataType.NUMBER ) @NotEmpty Double DenoiseLevel
			)
	{

		if("".equals(InputFilePath.trim()))
			throw new BotCommandException(MESSAGES.getString("emptyInputString", "InputFilePath"));

		if("".equals(OutputFilePath.trim()))
			throw new BotCommandException(MESSAGES.getString("emptyInputString", "OutputFilePath"));

		DllLoader l = new DllLoader();//Load OpenCV DLL

		FileProcessor fp = new FileProcessor(InputFilePath);

		ArrayList<String> AllImages = fp.GetFileAsImages();

		if(fp.isFileAPdf()){
			// actual OpenCV Logic
			for(String Image : AllImages){
				CommonCVOperations.Denoise(Image,Image,DenoiseLevel);
			}

			boolean res = fp.CombineImagesToPdf(AllImages,OutputFilePath);
			fp.Cleanup();
			return new BooleanValue(Boolean.toString(res));
		}else{
			CommonCVOperations.Denoise(AllImages.get(0),OutputFilePath,DenoiseLevel);
			boolean res = true;
			return new BooleanValue(Boolean.toString(res));
		}
	}

}
