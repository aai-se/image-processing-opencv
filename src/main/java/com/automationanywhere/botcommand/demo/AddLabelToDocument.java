/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.cvlogics.CommonCVOperations;
import com.automationanywhere.cvlogics.CommonGraphicsOperations;
import com.automationanywhere.utils.DllLoader;
import com.automationanywhere.utils.FileProcessor;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

/**
 * @author Bren Sapience
 *
 */
@BotCommand
@CommandPkg(label="Add Label", name="Add Label", description="Add Label", icon="pkg.svg",
		node_label="Add Label",
		return_type= DataType.BOOLEAN, return_label="Assign the output to variable", return_required=true)

public class AddLabelToDocument {

	private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

	@Execute
	public BooleanValue action(
			@Idx(index = "1", type = AttributeType.FILE) @Pkg(label = "Input File Path", default_value_type = DataType.FILE) @NotEmpty String InputFilePath,
			@Idx(index = "2", type = AttributeType.FILE) @Pkg(label = "Output File Path", default_value_type = DataType.FILE) @NotEmpty String OutputFilePath,
			@Idx(index = "3", type = AttributeType.NUMBER) @Pkg(label = "X Coordinate", default_value_type = DataType.NUMBER, default_value = "50") @NotEmpty Number x,
			@Idx(index = "4", type = AttributeType.NUMBER) @Pkg(label = "Y Coordinate", default_value_type = DataType.NUMBER, default_value = "50") @NotEmpty Number y,
			@Idx(index = "5", type = AttributeType.NUMBER) @Pkg(label = "Page Number (for multi-page PDFs only)", default_value_type = DataType.NUMBER, default_value = "1") @NotEmpty Number PageNum,
			@Idx(index = "6", type = AttributeType.TEXT) @Pkg(label = "Name of Label to add to document", default_value_type = DataType.STRING, default_value = "filename:") @NotEmpty String LabelName,
			@Idx(index = "7", type = AttributeType.TEXT) @Pkg(label = "Value to add to Label", default_value_type = DataType.STRING, default_value = "") @NotEmpty String LabelValue,
			@Idx(index = "8", type = TEXT) @Pkg(label = "Text Color (Hex)", default_value_type = STRING, default_value = "#0f0e0a",description = "ex: #0f0e0a")  String TextHexColor,
			@Idx(index = "9", type = AttributeType.NUMBER) @Pkg(label = "Font Size", default_value_type = DataType.NUMBER, default_value = "40") @NotEmpty Number FontSize
			) {

		if("".equals(String.valueOf(x)))
			throw new BotCommandException(MESSAGES.getString("emptyInputString", "x"));

		if("".equals(String.valueOf(y)))
			throw new BotCommandException(MESSAGES.getString("emptyInputString", "y"));

		if("".equals(InputFilePath.trim()))
			throw new BotCommandException(MESSAGES.getString("emptyInputString", "InputFilePath"));

		if("".equals(OutputFilePath.trim()))
			throw new BotCommandException(MESSAGES.getString("emptyInputString", "OutputFilePath"));

		Boolean isHexString = TextHexColor.matches("#[A-Za-z0-9]{6}");
		if(!isHexString) {
			throw new BotCommandException(MESSAGES.getString("wrongColorString", "BoundingBoxHexColor"));
		}

		int cropX = x.intValue();
		int cropY = y.intValue();

		DllLoader l = new DllLoader();//Load OpenCV DLL

		FileProcessor fp = new FileProcessor(InputFilePath);

		ArrayList<String> AllImages = fp.GetFileAsImages();

		int NumberOfPages = AllImages.size();
		if(PageNum.intValue()> NumberOfPages){
			throw new BotCommandException(MESSAGES.getString("pageNumberError", NumberOfPages));
		}

		if(fp.isFileAPdf()){
			String InputImagePath = AllImages.get(PageNum.intValue()-1);
			//System.out.println("DEBUG:"+InputImagePath);
			boolean Res = ProcessSingleImage(InputImagePath,LabelName, LabelValue,TextHexColor,x,y,FontSize);
			boolean res = fp.CombineImagesToPdf(AllImages,OutputFilePath);
			fp.Cleanup();
			return new BooleanValue(Boolean.toString(res));

		}else{

			boolean Res = ProcessSingleImage(InputFilePath,LabelName, LabelValue,TextHexColor,x,y,FontSize);
			return new BooleanValue(Boolean.toString(Res));
		}
	}

	private Boolean ProcessSingleImage(String PathToImage, String LabelName, String LabelValue, String TextColor, Number X, Number Y, Number FontSize){

		File InputSingleImageFile = new File(PathToImage);
		java.awt.Image InputSingleImage = null;

		try {
			InputSingleImage = ImageIO.read(InputSingleImageFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		BufferedImage BufferedInputImage = (BufferedImage) InputSingleImage;
		BufferedImage BufferedOutputFile = CommonGraphicsOperations.drawLabel(BufferedInputImage,LabelName, LabelValue, TextColor,X,Y,FontSize);
		try {
			ImageIO.write(BufferedOutputFile, "png", new File(PathToImage));
		} catch (IOException e) {
			e.printStackTrace();
		}

		boolean res = true;
		return res;

	}

}
