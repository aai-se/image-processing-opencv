/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.cvlogics.CommonCVOperations;
import com.automationanywhere.utils.DllLoader;
import com.automationanywhere.utils.FileProcessor;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "Sharpen Image", name = "sharpen image",
		description = "Sharpen Image",
		node_label = " Sharpen Image", icon = "pkg.svg",
		return_type= DataType.BOOLEAN, return_label="Assign the output to variable", return_required=true)
public class Sharpen  {

	private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

	@Execute
	public BooleanValue action (@Idx(index = "1", type = AttributeType.FILE)  @Pkg(label = "Input File Path" , default_value_type =  DataType.FILE) @NotEmpty String InputFilePath,
								@Idx(index = "2", type = AttributeType.FILE)  @Pkg(label = "Output File Path" , default_value_type =  DataType.FILE) @NotEmpty String OutputFilePath,
								@Idx(index = "3", type = AttributeType.NUMBER) @Pkg(label = "Gaussian Blur Sigma"  , default_value_type = DataType.NUMBER ) @NotEmpty Number sigma,
								@Idx(index = "4", type = AttributeType.NUMBER) @Pkg(label = "Weighted Alpha"  , default_value_type = DataType.NUMBER ) @NotEmpty Number alpha,
								@Idx(index = "5", type = AttributeType.NUMBER) @Pkg(label = "Weighted Beta"  , default_value_type = DataType.NUMBER ) @NotEmpty Number beta,
								@Idx(index = "6", type = AttributeType.NUMBER) @Pkg(label = "Weighted Gamma"  , default_value_type = DataType.NUMBER ) @NotEmpty Number gamma) throws Exception
	{

		if("".equals(InputFilePath.trim()))
			throw new BotCommandException(MESSAGES.getString("emptyInputString", "InputFilePath"));

		if("".equals(OutputFilePath.trim()))
			throw new BotCommandException(MESSAGES.getString("emptyInputString", "OutputFilePath"));


		DllLoader l = new DllLoader();//Load OpenCV DLL

		FileProcessor fp = new FileProcessor(InputFilePath);

		ArrayList<String> AllImages = fp.GetFileAsImages();

		if(fp.isFileAPdf()){
			// actual OpenCV Logic
			for(String Image : AllImages){
				CommonCVOperations.Sharpen(Image,Image,sigma,alpha,beta,gamma);
			}

			boolean res = fp.CombineImagesToPdf(AllImages,OutputFilePath);
			fp.Cleanup();
			return new BooleanValue(Boolean.toString(res));
		}else{
			CommonCVOperations.Sharpen(AllImages.get(0),OutputFilePath,sigma,alpha,beta,gamma);
			boolean res = true;
			return new BooleanValue(Boolean.toString(res));
		}

	}

}

