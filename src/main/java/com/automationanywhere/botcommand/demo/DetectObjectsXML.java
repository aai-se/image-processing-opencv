/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.cvlogics.CommonCVOperations;
import com.automationanywhere.utils.DllLoader;
import com.automationanywhere.utils.FileProcessor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Bren Sapience
 *
 */
@BotCommand
@CommandPkg(label="Detect Objects", name="Detect Objects", description="Detect Objects", icon="pkg.svg",
		node_label="Detect Objects",
		return_type= DataType.DICTIONARY, return_label="Assign the output to variable", return_required=true)

public class DetectObjectsXML {

	private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

	@Execute
	public DictionaryValue action(
			@Idx(index = "1", type = AttributeType.FILE) @Pkg(label = "Input File Path", default_value_type = DataType.FILE) @NotEmpty String InputFilePath,
			@Idx(index = "2", type = AttributeType.FILE) @Pkg(label = "Output File Path", default_value_type = DataType.FILE) String OutputFilePath,
			@Idx(index = "3", type = AttributeType.FILE)  @Pkg(label = "Cascade Classifier XML File" , default_value_type =  DataType.FILE) @NotEmpty String XMLObjectDetectionFile,
			@Idx(index = "4", type = AttributeType.NUMBER) @Pkg(label = "Number of Top Objects"  , default_value_type = DataType.NUMBER ) Number MaxNbOfTopObjects
			)
	{

		if("".equals(InputFilePath.trim()))
			throw new BotCommandException(MESSAGES.getString("emptyInputString", "InputFilePath"));


		String KEY1 = "number";
		Map<String,Value> ResMap = new LinkedHashMap();
		Value VALUE1 = new StringValue("0");
		ResMap.put(KEY1,VALUE1);




		DllLoader l = new DllLoader();//Load OpenCV DLL

		FileProcessor fp = new FileProcessor(InputFilePath);

		ArrayList<String> AllImages = fp.GetFileAsImages();

		if(fp.isFileAPdf()){
			// actual OpenCV Logic
			Map<String,Value> AllDetectedObjects = new LinkedHashMap();

			int TotalNumOfObjects = 0;

			for(String Image : AllImages){
				Map<String,Value> AllObjectsFromFile = CommonCVOperations.DetectObjectsFromXML(Image,Image,XMLObjectDetectionFile,MaxNbOfTopObjects);
				int NumberOfObjectDetected = Integer.parseInt(AllObjectsFromFile.get("number").toString());
				TotalNumOfObjects = TotalNumOfObjects + NumberOfObjectDetected;
				AllObjectsFromFile.remove("number");
				AllDetectedObjects.putAll(AllObjectsFromFile);
			}

			int FinalNumberOfObjects = TotalNumOfObjects;
			ResMap.replace(KEY1,new StringValue(Integer.toString(FinalNumberOfObjects)));
			ResMap.putAll(AllDetectedObjects);

			boolean res = true;
			if(!OutputFilePath.equals("")){
				res = fp.CombineImagesToPdf(AllImages,OutputFilePath);
			}

			fp.Cleanup();
			return new DictionaryValue(ResMap);

		}else{
			Map<String,Value> AllObjects = CommonCVOperations.DetectObjectsFromXML(AllImages.get(0),AllImages.get(0),XMLObjectDetectionFile,MaxNbOfTopObjects);
			boolean res = true;
			return new DictionaryValue(AllObjects);
		}
	}

}
