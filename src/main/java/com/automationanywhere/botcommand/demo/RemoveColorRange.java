/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.demo;


import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.cvlogics.CommonCVOperations;
import com.automationanywhere.utils.DllLoader;
import com.automationanywhere.utils.FileProcessor;


import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.cvlogics.CommonCVOperations;
import com.automationanywhere.utils.DllLoader;
import com.automationanywhere.utils.FileProcessor;

import java.util.ArrayList;

/**
 * @author Bren Sapience
 *
 */
@BotCommand
@CommandPkg(label="Remove Color Range", name="Remove Color Range", description="Remove Color Range", icon="pkg.svg",
		node_label="Remove Color Range",
		return_type= DataType.BOOLEAN, return_label="Assign the output to variable", return_required=true)

public class RemoveColorRange {

	private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

	@Execute
	public BooleanValue action(
			@Idx(index = "1", type = AttributeType.FILE) @Pkg(label = "Input File Path", default_value_type = DataType.FILE) @NotEmpty String InputFilePath,
			@Idx(index = "2", type = AttributeType.FILE) @Pkg(label = "Output File Path", default_value_type = DataType.FILE) @NotEmpty String OutputFilePath,
			@Idx(index = "3", type = AttributeType.NUMBER) @Pkg(label = "Lower Bound for Red", description = "defines the lower bound for range of pixels to keep (value between 0 and 255)",default_value_type = DataType.NUMBER) @NotEmpty Double LowerRed,
			@Idx(index = "4", type = AttributeType.NUMBER) @Pkg(label = "Higher Bound for Red", description = "defines the higher bound for range of pixels to keep (value between 0 and 255)",default_value_type = DataType.NUMBER,default_value = "255") @NotEmpty Double HigherRed,
			@Idx(index = "5", type = AttributeType.NUMBER) @Pkg(label = "Lower Bound for Blue", description = "defines the lower bound for range of pixels to keep (value between 0 and 255)",default_value_type = DataType.NUMBER) @NotEmpty Double LowerBlue,
			@Idx(index = "6", type = AttributeType.NUMBER) @Pkg(label = "Higher Bound for Blue", description = "defines the higher bound for range of pixels to keep (value between 0 and 255)",default_value_type = DataType.NUMBER,default_value = "255") @NotEmpty Double HigherBlue,
			@Idx(index = "7", type = AttributeType.NUMBER) @Pkg(label = "Lower Bound for Green", description = "defines the lower bound for range of pixels to keep (value between 0 and 255)",default_value_type = DataType.NUMBER) @NotEmpty Double LowerGreen,
			@Idx(index = "8", type = AttributeType.NUMBER) @Pkg(label = "Higher Bound for Green", description = "defines the higher bound for range of pixels to keep (value between 0 and 255)",default_value_type = DataType.NUMBER,default_value = "255") @NotEmpty Double HigherGreen
			)
	{

		if(LowerRed<0 || LowerRed>255){ throw new BotCommandException(MESSAGES.getString("wrongPixelValue", LowerRed));}
		if(LowerBlue<0 || LowerBlue>255){ throw new BotCommandException(MESSAGES.getString("wrongPixelValue", LowerBlue));}
		if(LowerGreen<0 || LowerGreen>255){ throw new BotCommandException(MESSAGES.getString("wrongPixelValue", LowerGreen));}

		DllLoader l = new DllLoader();//Load OpenCV DLL

		FileProcessor fp = new FileProcessor(InputFilePath);

		ArrayList<String> AllImages = fp.GetFileAsImages();

		if(fp.isFileAPdf()){
			// actual OpenCV Logic
			for(String Image : AllImages){
				CommonCVOperations.RemoveColorRange(Image,Image,LowerRed, HigherRed,LowerBlue, HigherBlue,LowerGreen, HigherGreen);
			}

			boolean res = fp.CombineImagesToPdf(AllImages,OutputFilePath);
			fp.Cleanup();
			return new BooleanValue(Boolean.toString(res));
		}else{
			CommonCVOperations.RemoveColorRange(AllImages.get(0),OutputFilePath,LowerRed, HigherRed,LowerBlue, HigherBlue,LowerGreen, HigherGreen);
			boolean res = true;
			return new BooleanValue(Boolean.toString(res));
		}
	}

}
