/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.cvlogics.CommonCVOperations;
import com.automationanywhere.utils.DllLoader;
import com.automationanywhere.utils.FileProcessor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Bren Sapience
 *
 */
@BotCommand
@CommandPkg(label="Get Blur", name="Get Blur", description="Get Blur of Image", icon="pkg.svg",
		node_label="Blur",
		return_type= DataType.LIST, return_label="Assign the output to variable", return_required=true)

public class GetBlur {

	private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

	@Execute
	public ListValue action(
			@Idx(index = "1", type = AttributeType.FILE) @Pkg(label = "Input File Path", default_value_type = DataType.FILE) @NotEmpty String InputFilePath
		) {

		if("".equals(InputFilePath.trim()))
			throw new BotCommandException(MESSAGES.getString("emptyInputString", "InputFilePath"));

		DllLoader l = new DllLoader();//Load OpenCV DLL

		FileProcessor fp = new FileProcessor(InputFilePath);

		ArrayList<String> AllImages = fp.GetFileAsImages();
		List<NumberValue> AllVariants = new ArrayList();
		if(fp.isFileAPdf()){
			// actual OpenCV Logic
			for(String Image : AllImages){
				String RawScore = CommonCVOperations.GetBlur(Image);
				//Number score = Integer.parseInt(RawScore);

				NumberValue nv = new NumberValue(RawScore);
				AllVariants.add(nv);
			}

			//double res = fp.CombineImagesToPdf(AllImages,OutputFilePath);
			fp.Cleanup();
			//return new BooleanValue(Boolean.toString(res));
		}else{
			//CommonCVOperations.GetBlur(AllImages.get(0));
			String RawScore = CommonCVOperations.GetBlur(AllImages.get(0));
			//Number score = Integer.parseInt(RawScore);

			NumberValue nv = new NumberValue(RawScore);
			AllVariants.add(nv);

			}

		//return new StringValue(FinResponse);
		ListValue myListValue = new ListValue();
		myListValue.set(AllVariants);
		return myListValue;
	}

}
