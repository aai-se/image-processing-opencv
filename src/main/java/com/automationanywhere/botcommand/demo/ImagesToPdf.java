/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.demo;


import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.cvlogics.CommonCVOperations;
import com.automationanywhere.utils.DllLoader;
import com.automationanywhere.utils.FileProcessor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.util.ArrayList;
import java.util.List;

/**
 * @author Bren Sapience
 *
 */
@BotCommand
@CommandPkg(label="Convert Images to PDF", name="Convert Images to PDF", description="Convert Images to PDF", icon="pkg.svg",
		node_label="Convert Images to PDF",
		return_type= DataType.BOOLEAN, return_label="boolean result", return_required=true)

public class ImagesToPdf {

	private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");
	private static final Logger logger = LogManager.getLogger(ImagesToPdf.class);


	@Execute
	public BooleanValue action(
			@Idx(index = "1", type = AttributeType.LIST) @Pkg(label = "List of Image Paths", default_value_type = DataType.LIST) @NotEmpty ArrayList<Value> InputFilePaths,
			@Idx(index = "2", type = AttributeType.FILE) @Pkg(label = "Output PDF File", default_value_type = DataType.FILE) @NotEmpty String OutputFilePath
			)
	{

		if(InputFilePaths.size()==0){ throw new BotCommandException(MESSAGES.getString("noImageInList"));}
		logger.info("OpenCV Init OK.") ;
		DllLoader l = new DllLoader();//Load OpenCV DLL
		ArrayList<String> AllImages = new ArrayList<String>();
		FileProcessor fp = new FileProcessor();

		for(Value value : InputFilePaths){
			String filepath = value.toString();
			if(fp.isFileAnImage(filepath)){
				AllImages.add(filepath);
			}else{
				// Image Not Supported?
			}
		}
		boolean res = fp.CombineImagesToPdf(AllImages,OutputFilePath);
		fp.Cleanup();
		return new BooleanValue(res);
	}

}
