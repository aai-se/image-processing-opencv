/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.cvlogics.CommonCVOperations;
import com.automationanywhere.utils.DllLoader;
import com.automationanywhere.utils.FileProcessor;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;

import static com.automationanywhere.commandsdk.model.DataType.STRING;

/**
 * @author Bren Sapience
 *
 */
@BotCommand
@CommandPkg(label="Crop", name="Crop", description="Crop Image", icon="pkg.svg",
		node_label="Crop",
		return_type= DataType.BOOLEAN, return_label="Assign the output to variable", return_required=true)

public class Crop {

	private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

	@Execute
	public BooleanValue action(
			@Idx(index = "1", type = AttributeType.FILE) @Pkg(label = "Input File Path", default_value_type = DataType.FILE) @NotEmpty String InputFilePath,
			@Idx(index = "2", type = AttributeType.FILE) @Pkg(label = "Output File Path", default_value_type = DataType.FILE) @NotEmpty String OutputFilePath,
			@Idx(index = "3", type = AttributeType.NUMBER) @Pkg(label = "X Coordinate", default_value_type = DataType.NUMBER) @NotEmpty Number x,
			@Idx(index = "4", type = AttributeType.NUMBER) @Pkg(label = "Y Coordinate", default_value_type = DataType.NUMBER) @NotEmpty Number y,
			@Idx(index = "5", type = AttributeType.NUMBER) @Pkg(label = "Width", default_value_type = DataType.NUMBER) @NotEmpty Number width,
			@Idx(index = "6", type = AttributeType.NUMBER) @Pkg(label = "Height", default_value_type = DataType.NUMBER) @NotEmpty Number height
			)
	{

		if("".equals(String.valueOf(x)))
			throw new BotCommandException(MESSAGES.getString("emptyInputString", "x"));

		if("".equals(String.valueOf(y)))
			throw new BotCommandException(MESSAGES.getString("emptyInputString", "y"));

		if("".equals(String.valueOf(width)))
			throw new BotCommandException(MESSAGES.getString("emptyInputString", "width"));

		if("".equals(String.valueOf(height)))
			throw new BotCommandException(MESSAGES.getString("emptyInputString", "height"));

		if("".equals(InputFilePath.trim()))
			throw new BotCommandException(MESSAGES.getString("emptyInputString", "InputFilePath"));

		if("".equals(OutputFilePath.trim()))
			throw new BotCommandException(MESSAGES.getString("emptyInputString", "OutputFilePath"));

		int cropHeight = height.intValue();
		int cropWidth = width.intValue();
		int cropX = x.intValue();
		int cropY = y.intValue();

		DllLoader l = new DllLoader();//Load OpenCV DLL

		FileProcessor fp = new FileProcessor(InputFilePath);

		ArrayList<String> AllImages = fp.GetFileAsImages();

		if(fp.isFileAPdf()){
			// actual OpenCV Logic
			for(String Image : AllImages){
				CommonCVOperations.CropImage(Image,Image,cropX, cropY,cropWidth,cropHeight);
			}

			boolean res = fp.CombineImagesToPdf(AllImages,OutputFilePath);
			fp.Cleanup();
			return new BooleanValue(Boolean.toString(res));
		}else{
			CommonCVOperations.CropImage(AllImages.get(0),OutputFilePath,cropX, cropY,cropWidth,cropHeight);
			boolean res = true;
			return new BooleanValue(Boolean.toString(res));
		}
	}

}
