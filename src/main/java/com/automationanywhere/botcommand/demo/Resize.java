/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.cvlogics.CommonCVOperations;
import com.automationanywhere.utils.DllLoader;
import com.automationanywhere.utils.FileProcessor;

import java.util.ArrayList;

import static com.automationanywhere.commandsdk.model.DataType.STRING;

/**
 * @author Bren Sapience
 *
 */
@BotCommand
@CommandPkg(label="Resize", name="Resize", description="Resize Image", icon="pkg.svg",
		node_label="Resize",
		return_type= DataType.BOOLEAN, return_label="Assign the output to variable", return_required=true)

public class Resize {

	private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");
	private static final String LABELABSOLUTE = "ABSOLUTE";
	private static final String LABELRELATIVE = "RELATIVE";

	@Execute
	public BooleanValue action
			/*
			(
			@Idx(index = "1", type = AttributeType.FILE) @Pkg(label = "Input File Path", default_value_type = DataType.FILE) @NotEmpty String InputFilePath,
			@Idx(index = "2", type = AttributeType.FILE) @Pkg(label = "Output File Path", default_value_type = DataType.FILE) @NotEmpty String OutputFilePath,

			@Idx(index = "3", type = AttributeType.NUMBER) @Pkg(label = "New Width (leave blank if unchanged)", default_value_type = DataType.NUMBER) Double newWidth,
			@Idx(index = "4", type = AttributeType.NUMBER) @Pkg(label = "New Height (leave blank if unchanged)", default_value_type = DataType.NUMBER)  Double newHeight
			)
			*/
	(
			@Idx(index = "1", type = AttributeType.FILE) @Pkg(label = "Input File Path", default_value_type = DataType.FILE) @NotEmpty String InputFilePath,
			@Idx(index = "2", type = AttributeType.FILE) @Pkg(label = "Output File Path", default_value_type = DataType.FILE) @NotEmpty String OutputFilePath,
			@Idx(index = "3", type = AttributeType.SELECT, options = {
					//Check the index of options. TO make them child we add a "." after parent index and start the indexing from 1.
					@Idx.Option(index = "3.1", pkg = @Pkg(label = "Absolute Resize", value = LABELABSOLUTE)),
					@Idx.Option(index = "3.2", pkg = @Pkg(label = "Percentage Resize", value = LABELRELATIVE))})
			@Pkg(label = "Select a resizing mode", description = "", default_value = "ABSOLUTE", default_value_type = STRING)
			@NotEmpty
			String ResizeMode,

	//Present the italian dishes
	//Check the index, its 1.1.1 meaning it would presented when 1.1 is selected
	 @Idx(index = "3.1.1", type = AttributeType.NUMBER) @Pkg(label = "New Width (Pixel)", default_value_type = DataType.NUMBER) Double newWidth,
	 @Idx(index = "3.1.2", type = AttributeType.NUMBER) @Pkg(label = "New Height (Pixel)", default_value_type = DataType.NUMBER)  Double newHeight,
	 @Idx(index = "3.2.1", type = AttributeType.NUMBER) @Pkg(label = "New Width (%)", default_value_type = DataType.NUMBER) Double newWidthRel,
	 @Idx(index = "3.2.2", type = AttributeType.NUMBER) @Pkg(label = "New Height (%)", default_value_type = DataType.NUMBER)  Double newHeightRel
	)

	{
		//System.out.println("DEBUG:["+newWidth+":"+newHeight+"]");
		if("".equals(InputFilePath.trim()))
			throw new BotCommandException(MESSAGES.getString("emptyInputString", "InputFilePath"));

		if("".equals(OutputFilePath.trim()))
			throw new BotCommandException(MESSAGES.getString("emptyInputString", "OutputFilePath"));

		DllLoader l = new DllLoader();//Load OpenCV DLL

		FileProcessor fp = new FileProcessor(InputFilePath);

		ArrayList<String> AllImages = fp.GetFileAsImages();

		if(fp.isFileAPdf()){
			// actual OpenCV Logic
			for(String Image : AllImages){
				if(ResizeMode.equalsIgnoreCase(LABELABSOLUTE)){
					CommonCVOperations.ResizeImage(Image,Image,newWidth,newHeight);
				}
				if(ResizeMode.equalsIgnoreCase(LABELRELATIVE)){
					CommonCVOperations.ResizeImageWithPercentage(Image,Image,newWidthRel,newHeightRel);
				}

			}

			boolean res = fp.CombineImagesToPdf(AllImages,OutputFilePath);
			fp.Cleanup();
			return new BooleanValue(Boolean.toString(res));
		}else{
			if(ResizeMode.equalsIgnoreCase(LABELABSOLUTE)){
				CommonCVOperations.ResizeImage(AllImages.get(0),OutputFilePath,newWidth,newHeight);
			}
			if(ResizeMode.equalsIgnoreCase(LABELRELATIVE)){
				CommonCVOperations.ResizeImageWithPercentage(AllImages.get(0),OutputFilePath,newWidthRel,newHeightRel);
			}
			boolean res = true;
			return new BooleanValue(Boolean.toString(res));
		}
	}

}
