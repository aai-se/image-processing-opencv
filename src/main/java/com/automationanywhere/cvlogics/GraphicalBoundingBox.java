package com.automationanywhere.cvlogics;

import java.awt.*;
import java.awt.image.BufferedImage;

public class GraphicalBoundingBox {
    Graphics g = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB).getGraphics();

    int X;
    int Y;
    int WIDTH;
    int HEIGHT;
    String SCORE;
    int PAGE;
    String TYPE;
    String LABEL;

    String BoundingBoxLineColor = "";
    Number BoundingBoxLineThickness = 4;
    String LabelBoxBackgroundColor = "";
    String LabelBoxLineColor = "";
    Font LabelBoxFont = g.getFont();
    String LabelBoxTextSize = "";


    public GraphicalBoundingBox(int x, int y, int width, int height, String score, int page, String fieldtype){
        this.X = x;
        this.Y = y;
        this.WIDTH = width;
        this.HEIGHT = height;
        this.SCORE = score;
        this.PAGE = page;
        this.TYPE = fieldtype;

    }

    public Graphics2D DrawABox(Graphics2D g){
        // Bounding Box around Object

        g.setColor(hex2Rgb(this.BoundingBoxLineColor));
        g.drawRect(this.X, this.Y, this.WIDTH+2, this.HEIGHT+2);
        g.setFont(LabelBoxFont);
        // Bounding Box around Label
        g.setColor(hex2Rgb(this.LabelBoxLineColor));
        int LabelRectLength = g.getFontMetrics().stringWidth(this.LABEL)+8;
        g.fillRect(this.X-LabelRectLength, this.Y, LabelRectLength, this.HEIGHT+2);

        float thickness = this.BoundingBoxLineThickness.floatValue();
        Stroke stroke = new BasicStroke(thickness);
        g.setStroke(stroke);

        // Color of Text
        g.setColor(hex2Rgb(this.LabelBoxBackgroundColor));
        g.drawRect(this.X-LabelRectLength, this.Y, LabelRectLength, this.HEIGHT+2);
        g.drawString(this.LABEL, this.X-LabelRectLength+4, this.Y+this.HEIGHT-1);

        return g;
    }

    public Graphics2D AddLabel(Graphics2D g){
        // Bounding Box around Object

        g.setColor(hex2Rgb(this.BoundingBoxLineColor));
        g.drawRect(this.X, this.Y, this.WIDTH+2, this.HEIGHT+2);
        g.setFont(LabelBoxFont);
        // Bounding Box around Label
        g.setColor(hex2Rgb(this.LabelBoxLineColor));
        
        int LabelRectLength = g.getFontMetrics().stringWidth(this.LABEL)+8;
        g.fillRect(this.X-LabelRectLength, this.Y, LabelRectLength, this.HEIGHT+2);

        float thickness = this.BoundingBoxLineThickness.floatValue();
        Stroke stroke = new BasicStroke(thickness);
        g.setStroke(stroke);

        // Color of Text
        g.setColor(hex2Rgb(this.LabelBoxBackgroundColor));
        g.drawRect(this.X-LabelRectLength, this.Y, LabelRectLength, this.HEIGHT+2);
        g.drawString(this.LABEL, this.X-LabelRectLength+4, this.Y+this.HEIGHT-1);

        return g;
    }

    public static Color hex2Rgb(String colorStr) {
        int StrLength = colorStr.length();
        if(StrLength<8){
            return new Color(100,100,100);
        }else{
            return new Color(
                    Integer.valueOf( colorStr.substring( 1, 3 ), 16 ),
                    Integer.valueOf( colorStr.substring( 3, 5 ), 16 ),
                    Integer.valueOf( colorStr.substring( 5, 7 ), 16 ) );
        }

    }
}
